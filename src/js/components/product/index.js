import Swiper from 'swiper/dist/js/swiper.js'

export default function productSlider () {
  let swiperProduct = ''
  const sliderProduct = document.querySelector('.productImageSlider')
  if (document.contains(sliderProduct)) {
    swiperProduct = new Swiper('.productImageSlider', {
      slidesPerView: '1',
      spaceBetween: 0,
      loop: true
    })
  }

  if (document.contains(sliderProduct)) {
    var slideControls = Array.from(document.querySelectorAll('.productImageSliderThumb'))
    slideControls.map(function (el, i) {
      el.addEventListener('click', function () {
        slideControls.map((item) => {
          item.classList.remove('productImageSliderThumbActive')
        })
        swiperProduct.slideTo(i + 1)
        el.classList.add('productImageSliderThumbActive')
      })
    })
  }
}
