export default function search() {

  const searchButton = document.querySelector('.buttonSearch')
  const searchInputDiv = document.querySelector('.headerSearchInput')
  const searchInput = searchInputDiv.querySelector('input')
  const searchPanel = document.querySelector('.headerSearchPanel')

  searchButton.addEventListener('click', () => {
    searchInputDiv.classList.toggle('headerSearchInputIsShow')
    searchPanel.classList.toggle('headerSearchPanelIsShow')
  })

  searchInput.addEventListener('focus', () => {
    searchPanel.classList.add('headerSearchPanelIsShow')
  })
}